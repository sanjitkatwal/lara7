
<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('title') ? 'has-error': '' }}" for="title-field-1"> Title</label>
    <div class="col-sm-9">
        {!! Form::text('title', null, [
            'class'=>'col-xs-10 col-sm-5 form-control',
            'placeholder'=> 'Enter Title'
        ]) !!}
        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="status">Status</label>
    <div class="col-sm-9">
        {!! Form::select('status', ['1' => 'Active', '0' => 'In-active'], null) !!}
    </div>
</div>
<div class="space-4"></div>


<div class="clearfix form-actions">
    <div class="col-md-offset-3 col-md-9">
        <button class="btn btn-info" type="submit">
            <i class="icon-ok bigger-110"></i>
            {{ $button }}
        </button>

        &nbsp; &nbsp; &nbsp;
        <button class="btn" type="reset">
            <i class="icon-undo bigger-110"></i>
            Reset
        </button>
    </div>
</div>
