<div class="form-group">
    <div class="col-xs-12">

        <div class="table-responsive">
            <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th></th>
                    <th>Choose Page</th>
                    <th><button type="button" class="btn btn-sm btn-primary" id="page-html-loader-btn">+</button></th>
                </tr>
                </thead>

                <tbody id="page_row_wrapper" class="ui-sortable">

                @if ($data['row']->pages()->count() > 0)
                    @foreach($data['row']->pages()->orderBy('rank', 'asc')->get() as $menu_section_page)
                        @include('admin.menu-section.common.page_content_row')
                        @endforeach

                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
