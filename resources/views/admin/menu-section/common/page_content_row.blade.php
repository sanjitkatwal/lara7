<tr class="ui-sortable-handle">
    <td><i class="icon-move bigger-120"></i> </td>
    <td>
        <select name="pages[]" id="">
            @foreach($data['pages'] as $page)
                @if(isset($menu_section_page))
                    <option value="{{ $page->id }}" {{ $menu_section_page->id == $page->id?'selected':'' }}>{{ $page->title }}</option>
                @else
                <option value="{{ $page->id }}">{{ $page->title }}</option>
                    @endif
            @endforeach

        </select>
    </td>
    <td>
        <button type="button" class="btn btn-sm btn-danger page-row-remove-btn" onclick="$(this).closest('tr').remove()">X</button>
    </td>
</tr>

