@extends('admin.includes.layout')

@section('title')
    Category : Edit
@endsection

@section('content')
    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            <ul class="breadcrumb">
                @include('admin.includes.dashboard_breadcrumb_url')

                <li>
                    <a href="{{ route($_base_route) }}">{{ $_panel }}</a>
                </li>
                <li class="active">Edit</li>
            </ul><!-- .breadcrumb -->

        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    {{ $_panel }} Manager
                    <small>
                        <i class="icon-double-angle-right"></i>
                        Edit Form
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    {!! Form::model($data['row'], [
                        'url'   => route($_base_route.'.update', $data['row']->id),
                        'class' => 'form-horizontal',
                        'enctype'=> 'multipart/form-data'
                    ]) !!}

                    {!! Form::hidden('id', $data['row']->id) !!}

                    @include($_view_path.'.common.form', ['button' => 'Update'])

                    {!! Form::close() !!}
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
@endsection


@section('js')
    <script>
        $(document).ready(function () {
            $('#page-html-loader-btn').click(function () {
                $.ajax({
                    method: 'POST',
                    url: '{{ route('admin.menu-section.load-page-row') }}',
                    data: {
                        _token: '{{ @csrf_token() }}',
                    },
                    success: function (response) {
                        var data = $.parseJSON(response);
                        $('#page_row_wrapper').append(data.html)
                    }
                });
            })
        })
    </script>

    @include('admin.includes.js_scrollable')
@endsection

