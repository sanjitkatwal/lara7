<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('main_image') ? 'has-error': '' }}" for="image-field-1"> Slider Image</label>
    <div class="col-sm-9">
        {!! Form::file('main_image', ['classs'=>'form-control']) !!}
        @if ($errors->has('main_image'))
            <span class="help-block">
                <strong>{{ $errors->first('main_image') }}</strong>
            </span>

        @endif
    </div>

</div>
<div class="space-4"></div>

@if(isset($data['row']))
    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="image-field-1"> Existing Image</label>
        <div class="col-sm-9">
            @if ($data['row']->image)
                <img src="{{ asset('images/'.$_folder.'/'.$data['row']->image) }}" width="50" alt="">
            @else
                <p>No Image</p>
            @endif
        </div>
    </div>
    <div class="space-4"></div>
@endif
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('alt_text') ? 'has-error': '' }}" for="alt_text-field-1"> Alternative Text</label>
    <div class="col-sm-9">
        {!! Form::text('alt_text', null, [
            'class'=>'col-xs-10 col-sm-5 form-control',
            'placeholder'=> 'Enter Alternative Text'
        ]) !!}
        @if ($errors->has('alt_text'))
            <span class="help-block">
                <strong>{{ $errors->first('alt_text') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('caption') ? 'has-error': '' }}" for="caption-field-1"> Caption</label>
    <div class="col-sm-9">
        {!! Form::text('caption', null, [
            'class'=>'col-xs-10 col-sm-5 form-control',
            'placeholder'=> 'Enter Caption'
        ]) !!}
        @if ($errors->has('caption'))
            <span class="help-block">
                <strong>{{ $errors->first('caption') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('link') ? 'has-error': '' }}" for="link-field-1"> Link</label>
    <div class="col-sm-9">
        {!! Form::textarea('link', null, ['rows' => 3, 'class'=>'form-control']) !!}
        @if ($errors->has('link'))
            <span class="help-block">
                <strong>{{ $errors->first('link') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('rank') ? 'has-error': '' }}" for="rank-field-1"> Rank</label>
    <div class="col-sm-9">
        {!! Form::text('rank', null, [
            'class'=>'col-xs-10 col-sm-5 form-control',
            'placeholder'=> '1-n'
        ]) !!}
        @if ($errors->has('rank'))
            <span class="help-block">
                <strong>{{ $errors->first('rank') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="status">Status</label>
    <div class="col-sm-9">
        {!! Form::select('status', ['1' => 'Active', '0' => 'In-active'], null) !!}
    </div>
</div>
<div class="space-4"></div>


<div class="clearfix form-actions">
    <div class="col-md-offset-3 col-md-9">
        <button class="btn btn-info" type="submit">
            <i class="icon-ok bigger-110"></i>
            {{ $button }}
        </button>

        &nbsp; &nbsp; &nbsp;
        <button class="btn" type="reset">
            <i class="icon-undo bigger-110"></i>
            Reset
        </button>
    </div>
</div>
