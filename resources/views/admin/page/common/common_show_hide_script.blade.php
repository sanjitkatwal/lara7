<script>
    $(document).ready(function(){
        init_page_type();

        $('#page_type').change(function(){
            var type = $(this).val();
            if(type == 'content_page') {
                $('.content_type_section').show()
                $('.link_type_section').hide()
            }else{
                $('.link_type_section').show()
                $('.content_type_section').hide()
            }
        })
    });


    function init_page_type() {
        var type = $('#page_type').val();
        if(type == 'content_page') {
            $('.content_type_section').show()
            $('.link_type_section').hide()
        }else{
            $('.link_type_section').show()
            $('.content_type_section').hide()
        }
    }
</script>
