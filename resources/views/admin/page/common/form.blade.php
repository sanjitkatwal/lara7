<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('page_type') ? 'has-error':'' }}" for="page_type">Choose Page Type</label>
    <div class="col-sm-9">
        {!! Form::select('page_type', ['content_page' => 'Content Page', 'link_page' => 'Link Page'], null,
        ['class' => 'col-sm-6', 'id' => 'page_type']) !!}

        @if ($errors->has('page_type'))
            <span class="help-block">
                <strong>{{ $errors->first('page_type') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('title') ? 'has-error': '' }}" for="title-field-1"> Page Title</label>
    <div class="col-sm-9">
        {!! Form::text('title', null, [
            'class'=>'col-sm-6',
            'placeholder'=> 'Enter Alternative Text'
        ]) !!}
        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>


<div class="form-group content_type_section"  style="display: none">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('main_image') ? 'has-error': '' }}" for="image-field-1"> Main Image</label>
    <div class="col-sm-9">
        {!! Form::file('main_image', ['classs'=>'form-control']) !!}
        @if ($errors->has('main_image'))
            <span class="help-block">
                <strong>{{ $errors->first('main_image') }}</strong>
            </span>

        @endif
    </div>

</div>
<div class="space-4"></div>

@if(isset($data['row']))
    <div class="form-group content_type_section"  style="display: none">
        <label class="col-sm-3 control-label no-padding-right" for="image-field-1"> Existing Image</label>
        <div class="col-sm-9">
            @if ($data['row']->image)
                <img src="{{ asset('images/'.$_folder.'/'.$data['row']->image) }}" width="50" alt="">
            @else
                <p>No Image</p>
            @endif
        </div>
    </div>
    <div class="space-4"></div>
@endif
<div class="space-4"></div>

<div class="form-group content_type_section"  style="display: none">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('caption') ? 'has-error': '' }}" for="caption-field-1"> Description</label>
    <div class="col-sm-9">
        {!! Form::textarea('description', null, ['rows' => 10, 'class'=>'form-control']) !!}
        @if ($errors->has('caption'))
            <span class="help-block">
                <strong>{{ $errors->first('caption') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="space-4"></div>

<div class="form-group link_type_section" style="display: none">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('link') ? 'has-error': '' }}" for="link-field-1"> Link</label>
    <div class="col-sm-9">
        {!! Form::text('link', null, [
            'class'=>'col-sm-6',
            'placeholder'=> 'Enter Link'
        ]) !!}
        @if ($errors->has('link'))
            <span class="help-block">
                <strong>{{ $errors->first('link') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="status">Status</label>
    <div class="col-sm-9">
        {!! Form::select('status', ['1' => 'Active', '0' => 'In-active'], null) !!}
    </div>
</div>
<div class="space-4"></div>


<div class="clearfix form-actions">
    <div class="col-md-offset-3 col-md-9">
        <button class="btn btn-info" type="submit">
            <i class="icon-ok bigger-110"></i>
            {{ $button }}
        </button>

        &nbsp; &nbsp; &nbsp;
        <button class="btn" type="reset">
            <i class="icon-undo bigger-110"></i>
            Reset
        </button>
    </div>
</div>
