@extends('admin.includes.layout')

@section('title')
    User : Profile
@endsection

@section('content')
    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            <ul class="breadcrumb">
                @include('admin.includes.dashboard_breadcrumb_url')

                <li>
                    <a href="#">Forms</a>
                </li>
                <li class="active">Form Elements</li>
            </ul><!-- .breadcrumb -->

            <div class="nav-search" id="nav-search">
                <form class="form-search">
                    <span class="input-icon">
                        <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off">
                        <i class="icon-search nav-search-icon"></i>
                    </span>
                </form>
            </div><!-- #nav-search -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    Form Elements
                    <small>
                        <i class="icon-double-angle-right"></i>
                        Common form elements and layouts
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    @if(session()->has('success_message'))
                        <div class="alert alert-block alert-success">
                            <button type="button" class="close" data-dismiss="alert">
                                <i class="icon-remove"></i>
                            </button>

                            <p>
                                <strong>
                                    <i class="icon-ok"></i>
                                    Well done!
                                </strong>
                                {{ session()->get('success_message') }}
                            </p>

                        </div>
                    @endif
                    <form class="form-horizontal" role="form" method="post" action="{{ route('admin.profile.update') }}">
                        @csrf
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right {{ $errors->has('name') ? 'has-error': '' }}" for="name-field-1"> User Name</label>

                            <div class="col-sm-9">
                                <input type="text" id="form-field-1" name="name" value="{{ $data['user']->name }}" placeholder="Username" class="col-xs-10 col-sm-5">
                            </div>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong></strong>{{ $errors->first('name') }}</strong>
                                </span>

                            @endif
                        </div>
                        <div class="space-4"></div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right {{ $errors->has('first_name') ? 'has-error': '' }}" for="name-field-1"> First Name</label>

                            <div class="col-sm-9">
                                <input type="text" id="form-field-1" name="first_name" value="{{ $data['user']->first_name }}" placeholder="First Name" class="col-xs-10 col-sm-5">
                            </div>
                            @if ($errors->has('first_name'))
                                <span class="help-block">
                                    <strong></strong>{{ $errors->first('first_name') }}</strong>
                                </span>

                            @endif
                        </div>
                        <div class="space-4"></div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right {{ $errors->has('middle_name') ? 'has-error': '' }}" for="middle_name-field-1"> Middle Name</label>

                            <div class="col-sm-9">
                                <input type="text" id="form-field-1" name="middle_name" value="{{ $data['user']->middle_name }}" placeholder="Middle Name" class="col-xs-10 col-sm-5">
                            </div>
                            @if ($errors->has('middle_name'))
                                <span class="help-block">
                                    <strong></strong>{{ $errors->first('middle_name') }}</strong>
                                </span>

                            @endif
                        </div>
                        <div class="space-4"></div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right {{ $errors->has('last_name') ? 'has-error': '' }}" for="last_name-field-1"> Last Name</label>

                            <div class="col-sm-9">
                                <input type="text" id="form-field-1" name="last_name" value="{{ $data['user']->last_name }}" placeholder="Last Name" class="col-xs-10 col-sm-5">
                            </div>
                            @if ($errors->has('last_name'))
                                <span class="help-block">
                                    <strong></strong>{{ $errors->first('last_name') }}</strong>
                                </span>

                            @endif
                        </div>
                        <div class="space-4"></div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right {{ $errors->has('email') ? 'has-error': '' }}" for="name-field-1"> Email</label>

                            <div class="col-sm-9">
                                <input type="email" id="form-field-1" name="email" value="{{ $data['user']->email }}" placeholder="Email" class="col-xs-10 col-sm-5">
                            </div>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong></strong>{{ $errors->first('email') }}</strong>
                                </span>

                            @endif
                        </div>
                        <div class="space-4"></div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right {{ $errors->has('contact') ? 'has-error': '' }}" for="last_name-field-1"> Contact</label>

                            <div class="col-sm-9">
                                <input type="number" id="form-field-1" name="contact" value="{{ $data['user']->contact }}" placeholder="Contact" class="col-xs-10 col-sm-5">
                            </div>
                            @if ($errors->has('contact'))
                                <span class="help-block">
                                    <strong></strong>{{ $errors->first('contact') }}</strong>
                                </span>

                            @endif
                        </div>
                        <div class="space-4"></div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right {{ $errors->has('address') ? 'has-error': '' }}" for="address-field-1"> Address</label>

                            <div class="col-sm-9">
                                <input type="text" id="form-field-1" name="contact" value="{{ $data['user']->address }}" placeholder="Address" class="col-xs-10 col-sm-5">
                            </div>
                            @if ($errors->has('address'))
                                <span class="help-block">
                                    <strong></strong>{{ $errors->first('address') }}</strong>
                                </span>

                            @endif
                        </div>
                        <div class="space-4"></div>
                        <hr>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right {{ $errors->has('password') ? 'has-error': '' }}" for="password"> Password</label>

                            <div class="col-sm-9">
                                <input type="password" id="form-field-1" name="password" placeholder="" class="col-xs-10 col-sm-5">
                            </div>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong></strong>{{ $errors->first('password') }}</strong>
                                </span>

                            @endif
                        </div>
                        <div class="space-4"></div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="password_confirmation"> Confirm Password</label>

                            <div class="col-sm-9">
                                <input type="password" id="form-field-1" name="password_confirmation" placeholder="" class="col-xs-10 col-sm-5">
                            </div>
                        </div>
                        <div class="space-4"></div>

                        <div class="clearfix form-actions">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn btn-info" type="submit">
                                    <i class="icon-ok bigger-110"></i>
                                    Update
                                </button>

                                <button class="btn" type="reset">
                                    <i class="icon-undo bigger-110"></i>
                                    Reset
                                </button>
                            </div>
                        </div>

                    </form>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
@endsection

