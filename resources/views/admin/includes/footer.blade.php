
<!--[if !IE]> -->

<script type="text/javascript">
    window.jQuery || document.write("<script src='{{ asset('admin/assets/js/jquery-2.0.3.min.js')}}'>"+"<"+"/script>");
</script>

<!-- <![endif]-->

<!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='{{ asset('admin/assets/js/jquery-1.10.2.min.js') }}'>"+"<"+"/script>");
</script>
<![endif]-->

<script type="text/javascript">
    if("ontouchend" in document) document.write("<script src='{{ asset('admin/assets/js/jquery.mobile.custom.min.js') }}'>"+"<"+"/script>");
</script>
<script src="{{ asset('admin/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('admin/assets/js/typeahead-bs2.min.js') }}"></script>
<!--[if lte IE 8]>
<script src="{{ asset('admin/assets/js/excanvas.min.js') }}"></script>
<![endif]-->

<!-- ace scripts -->

<script src="{{ asset('admin/assets/js/ace-elements.min.js') }}"></script>
<script src="{{ asset('admin/assets/js/ace.min.js') }}"></script>

<!-- inline scripts related to this page -->


@yield('js')

<script src="{{ asset('admin/assets/js/bootbox.min.js') }}"></script>
<script>
    $(document).ready(function(){

        $(".bootbox-confirm").on('click', function() {

            var $this = $(this);
            bootbox.confirm("Are you sure?", function(result) {
                if(result) {
                    location.href = $this.attr('href');
                }
            });

            return false;
        });

    })
</script>

</body>

<!-- Mirrored from 198.74.61.72/themes/preview/ace/ by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 28 Feb 2014 17:45:06 GMT -->
</html>
