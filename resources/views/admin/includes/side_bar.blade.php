<div class="sidebar" id="sidebar">
    <script type="text/javascript">
        try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
    </script>

    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <button class="btn btn-success">
                <i class="icon-signal"></i>
            </button>

            <button class="btn btn-info">
                <i class="icon-pencil"></i>
            </button>

            <button class="btn btn-warning">
                <i class="icon-group"></i>
            </button>

            <button class="btn btn-danger">
                <i class="icon-cogs"></i>
            </button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>
    </div><!-- #sidebar-shortcuts -->

    <ul class="nav nav-list">
        <li {!! request()->is('admin/dashboard')?'class="active"':'' !!}>
            <a href="{{ route('admin.dashboard') }}">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> Dashboard </span>
            </a>
        </li>

        <li {!! request()->is('admin/post*')?'class="active"':'' !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-desktop"></i>
                <span class="menu-text"> Post Manager </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li {!! request()->is('admin/post')?'class="active"':'' !!}>
                    <a href="{{ route('admin.post') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>

                <li {!! request()->is('admin/post/create')?'class="active"':'' !!}>
                    <a href="{{ route('admin.post.create') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

        <li {!! request()->is('admin/user*')?'class="active"':'' !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-user"></i>
                <span class="menu-text"> User Manager </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li {!! request()->is('admin/user')?'class="active"':'' !!}>
                    <a href="{{ route('admin.user') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>

                <li {!! request()->is('admin/user/create')?'class="active"':'' !!}>
                    <a href="{{ route('admin.user.create') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

        <li {!! request()->is('admin/tag*')?'class="active"':'' !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-list"></i>
                <span class="menu-text"> Tag Manager </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li {!! request()->is('admin/tag')?'class="active"':'' !!}>
                    <a href="{{ route('admin.tag') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>

                <li {!! request()->is('admin/tag/create')?'class="active"':'' !!}>
                    <a href="{{ route('admin.tag.create') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

        <li {!! request()->is('admin/category*')?'class="active"':'' !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-list"></i>
                <span class="menu-text"> Category Manager </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li {!! request()->is('admin/category')?'class="active"':'' !!}>
                    <a href="{{ route('admin.category') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>

                <li {!! request()->is('admin/category/create')?'class="active"':'' !!}>
                    <a href="{{ route('admin.category.create') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

        <li {!! request()->is('admin/brand*')?'class="active"':'' !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-list"></i>
                <span class="menu-text"> Brand Manager </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li {!! request()->is('admin/brand')?'class="active"':'' !!}>
                    <a href="{{ route('admin.brand') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>

                <li {!! request()->is('admin/brand/create')?'class="active"':'' !!}>
                    <a href="{{ route('admin.brand.create') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

        <li {!! request()->is('admin/slider*')?'class="active"':'' !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-list"></i>
                <span class="menu-text"> Slider Manager </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li {!! request()->is('admin/slider')?'class="active"':'' !!}>
                    <a href="{{ route('admin.slider') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>

                <li {!! request()->is('admin/slider/create')?'class="active"':'' !!}>
                    <a href="{{ route('admin.slider.create') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

        <li {!! request()->is('admin/page*')?'class="active"':'' !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-list"></i>
                <span class="menu-text"> Page Manager </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li {!! request()->is('admin/page')?'class="active"':'' !!}>
                    <a href="{{ route('admin.page') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>

                <li {!! request()->is('admin/page/create')?'class="active"':'' !!}>
                    <a href="{{ route('admin.page.create') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

        <li {!! request()->is('admin/menu-section*')?'class="active"':'' !!}>
            <a href="{{ route('admin.menu-section') }}">
                <i class="icon-list"></i>
                <span class="menu-text"> Menu Manager </span>
            </a>
        </li>

        <li {!! request()->is('admin/attribute-group*')?'class="active"':'' !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-list"></i>
                <span class="menu-text"> Attribute Manager </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li {!! request()->is('admin/attribute-group')?'class="active"':'' !!}>
                    <a href="{{ route('admin.attribute-group') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>

                <li {!! request()->is('admin/attribute-group/create')?'class="active"':'' !!}>
                    <a href="{{ route('admin.attribute-group.create') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

        <li {!! request()->is('admin/currency*')?'class="active"':'' !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-money"></i>
                <span class="menu-text"> Currency Manager </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li {!! request()->is('admin/currency')?'class="active"':'' !!}>
                    <a href="{{ route('admin.currency') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>

                <li {!! request()->is('admin/currency/create')?'class="active"':'' !!}>
                    <a href="{{ route('admin.currency.create') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>



    </ul><!-- /.nav-list -->

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>

    <script type="text/javascript">
        try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
    </script>
</div>
