<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('parent_id')?  'has-error':'' }}" for="parent_id">Parent Category</label>
    <div class="col-sm-9">

        {{--                                <select name="parent_id" class="col-xs-10 col-sm-5">--}}
        {{--                                    <option value="0">-- Is Parent --</option>--}}
        {{--                                    @if($data['parent_categories']->count() > 0)--}}
        {{--                                        @foreach($data['parent_categories'] as $category)--}}
        {{--                                            <option value="{{ $category->id }}">{{ $category->title }}</option>--}}
        {{--                                        @endforeach--}}
        {{--                                    @endif--}}
        {{--                                </select>--}}


        {!! Form::select('parent_id', $data['parent_categories'], null, [
            'class'=>'col-xs-10 col-sm-5'
        ]) !!}
    </div>
</div>
<div class="space-4"></div>
<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('title') ? 'has-error': '' }}" for="title-field-1"> Title</label>
    <div class="col-sm-9">
        {!! Form::text('title', null, [
            'class'=>'col-xs-10 col-sm-5 form-control',
            'placeholder'=> 'Enter Title'
        ]) !!}
        @if ($errors->has('title'))
            <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
        @endif
    </div>

</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('image') ? 'has-error': '' }}" for="image-field-1"> Banner Image</label>
    <div class="col-sm-9">
        {!! Form::file('image', ['classs'=>'form-control']) !!}
        @if ($errors->has('image'))
            <span class="help-block">
                <strong>{{ $errors->first('image') }}</strong>
            </span>

        @endif
    </div>

</div>
<div class="space-4"></div>

@if(isset($data['row']))
    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="image-field-1"> Existing Image</label>
        <div class="col-sm-9">
            @if ($data['row']->banner_image)
                <img src="{{ asset('images/'.$_folder.'/'.$data['row']->banner_image) }}" width="50" alt="">
                <label for="checkbox">
                    <input type="checkbox" name="remove_image">
                    <span>Remove Image</span>
                </label>
            @else
                <p>No Image</p>
            @endif
        </div>
    </div>
    <div class="space-4"></div>
@endif

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('description') ? 'has-error': '' }}" for="description-field-1"> Description</label>
    <div class="col-sm-9">
        {!! Form::textarea('description', null, ['rows' => 10, 'class'=>'form-control']) !!}
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="status">Status</label>
    <div class="col-sm-9">
        {!! Form::select('status', ['1' => 'Active', '0' => 'In-active'], null) !!}
    </div>
</div>
<div class="space-4"></div>


<div class="clearfix form-actions">
    <div class="col-md-offset-3 col-md-9">
        <button class="btn btn-info" type="submit">
            <i class="icon-ok bigger-110"></i>
            {{ $button }}
        </button>

        &nbsp; &nbsp; &nbsp;
        <button class="btn" type="reset">
            <i class="icon-undo bigger-110"></i>
            Reset
        </button>
    </div>
</div>
