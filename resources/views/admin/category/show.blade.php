@extends('admin.includes.layout')

@section('title')
    Category : Show
@endsection

@section('content')
    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            <ul class="breadcrumb">
                @include('admin.includes.dashboard_breadcrumb_url')

                <li>
                    <a href="{{ route($_base_route) }}">{{ $_panel }}</a>
                </li>
                <li class="active">View</li>
            </ul><!-- .breadcrumb -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    {{ $_panel }} Manager
                    <small>
                        <i class="icon-double-angle-right"></i>
                        Details
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    <div class="row">
                        <div class="col-xs-12">

                            <div class="table-responsive">
                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Column</th>
                                            <th>Value</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <th>Id</th>
                                            <td>
                                                {{ $data['row']->id }}
                                            </td>
                                        </tr>

                                        <tr>
                                            <th>Parent Id</th>
                                            <td>
                                                @if ($data['row']->parent_id == 0)
                                                    <p>Parent Category</p>
                                                    @else
                                                <p>{{ \App\Model\Category::find($data['row']->parent_id)->title }}</p>
                                                @endif
                                            </td>
                                        </tr>

                                        <tr>
                                            <th>Title</th>
                                            <td>
                                                {{ $data['row']->title }}
                                            </td>
                                        </tr>

                                        <tr>
                                            <th>Image</th>
                                            <td>
                                                @if ($data['row']->banner_image)
                                                    <img src="{{ asset('images/'.$_folder.'/'.$data['row']->banner_image) }}" width="150" alt="No Image Found">
                                                    @else
                                                <p>No Image Uploaded</p>
                                                @endif
                                            </td>
                                        </tr>

                                        <tr>
                                            <th>Description</th>
                                            <td>
                                                {!! $data['row']->description !!}
                                            </td>
                                        </tr>

                                        <tr>
                                            <th>Slug</th>
                                            <td>
                                                {!! $data['row']->slug !!}
                                            </td>
                                        </tr>

                                        <tr>
                                            <th>Status</th>
                                            <td>
                                                @if ($data['row']->status == 1)
                                                    <button class="btn btn-xs btn-success">Active</button>
                                                    @else
                                                    <button class="btn btn-xs btn-danger">In-Active</button>
                                                @endif

                                            </td>
                                        </tr>

                                        <tr>
                                            <th>Created Ar</th>
                                            <td>
                                                {!! $data['row']->created_at !!}
                                            </td>
                                        </tr>

                                        <tr>
                                            <th>Updated At</th>
                                            <td>
                                                {!! $data['row']->updated_at !!}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div><!-- /.table-responsive -->
                        </div><!-- /span -->
                    </div><!-- /row -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
@endsection
