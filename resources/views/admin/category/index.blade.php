@extends('admin.includes.layout')

@section('title')
    Category : Index
@endsection

@section('content')
    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            <ul class="breadcrumb">
               @include('admin.includes.dashboard_breadcrumb_url')

                <li>
                    <a href="{{ route($_base_route) }}">{{ $_panel }}</a>
                </li>
                <li class="active">List</li>
            </ul><!-- .breadcrumb -->

            <div class="nav-search" id="nav-search">
                <form class="form-search">
                    <span class="input-icon">
                        <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off">
                        <i class="icon-search nav-search-icon"></i>
                    </span>
                </form>
            </div><!-- #nav-search -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    {{ $_panel }} Manager
                    <small>
                        <i class="icon-double-angle-right"></i>
                        List Data
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    <div class="row">
                        <div class="col-xs-12">

                           @include('admin.includes.flash_messages')

                            <div class="table-responsive">
                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="center">
                                            <label>
                                                <input type="checkbox" class="ace">
                                                <span class="lbl"></span>
                                            </label>
                                        </th>
                                        <th>Title</th>
                                        <th>Banner Image</th>
                                        <th class="hidden-480">Description</th>

                                        <th class="hidden-480">Status</th>

                                        <th></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @if($data['rows']->count() > 0)
                                        @foreach($data['rows'] as $row)
                                            <tr>
                                                <td class="center">
                                                    <label>
                                                        <input type="checkbox" class="ace">
                                                        <span class="lbl"></span>
                                                    </label>
                                                </td>

                                                <td>
                                                    <a href="#">
                                                        @if ($row->parent)
                                                            {!!  $row->parent . '&nbsp;>>&nbsp;'. $row->title !!}
                                                        @else
                                                            {{ $row->title }}
                                                        @endif
                                                    </a>
                                                </td>
                                                <td>
                                                    @if ($row->banner_image)
                                                        <img src="{{ asset('images/'.$_folder.'/'.$row->banner_image) }}" width="150" alt="">
                                                        @else
                                                    <p>No Image</p>
                                                    @endif
                                                </td>
                                                <td class="hidden-480">{{ $row->description }}</td>
                                                 <td class="hidden-480">
                                                    <span class="label label-sm label-warning">{{ $row->status }}</span>
                                                </td>

                                                <td>
                                                    <div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
                                                        <a href="{{ route($_base_route.'.show', $row->id) }}" class="btn btn-xs btn-success">
                                                            <i class="icon-ok bigger-120"></i>
                                                        </a>

                                                        <a href="{{ route($_base_route.'.edit', $row->id) }}" class="btn btn-xs btn-info">
                                                            <i class="icon-edit bigger-120"></i>
                                                        </a>

                                                        <a href="{{ route($_base_route.'.delete', $row->id) }}" class="btn btn-xs btn-danger">
                                                            <i class="icon-trash bigger-120"></i>
                                                        </a>
                                                    </div>

                                                </td>
                                            </tr>
                                        @endforeach
                                        @else
                                    <tr>
                                        <td colspan="6">No Data Found</td>
                                    </tr>
                                    @endif

                                    </tbody>
                                </table>
                            </div><!-- /.table-responsive -->
                        </div><!-- /span -->
                    </div><!-- /row -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
@endsection
