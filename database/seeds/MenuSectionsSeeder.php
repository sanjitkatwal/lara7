<?php

use Illuminate\Database\Seeder;

class MenuSectionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('menu_sections')->insert([
            'title' => 'Top Navigation',
            'slug' => 'top-nav',
            'hint' => null,
            'created_at' => Carbon\Carbon::now()
        ]);

        \DB::table('menu_sections')->insert([
            'title' => 'Navigation',
            'slug' => 'nav-bar',
            'hint' => null,
            'created_at' => Carbon\Carbon::now()
        ]);

        \DB::table('menu_sections')->insert([
            'title' => 'Footer1',
            'slug' => 'footer-1',
            'hint' => null,
            'created_at' => Carbon\Carbon::now()
        ]);

        \DB::table('menu_sections')->insert([
            'title' => 'Footer2',
            'slug' => 'footer-2',
            'hint' => null,
            'created_at' => Carbon\Carbon::now()
        ]);

        \DB::table('menu_sections')->insert([
            'title' => 'Footer3',
            'slug' => 'footer-3',
            'hint' => null,
            'created_at' => Carbon\Carbon::now()
        ]);

        \DB::table('menu_sections')->insert([
            'title' => 'Footer4',
            'slug' => 'footer-4',
            'hint' => null,
            'created_at' => Carbon\Carbon::now()
        ]);

        \DB::table('menu_sections')->insert([
            'title' => 'Footer5',
            'slug' => 'footer-5',
            'hint' => null,
            'created_at' => Carbon\Carbon::now()
        ]);

        \DB::table('menu_sections')->insert([
            'title' => 'Footer6',
            'slug' => 'footer-6',
            'hint' => null,
            'created_at' => Carbon\Carbon::now()
        ]);
    }
}
