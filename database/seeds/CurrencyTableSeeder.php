<?php

use Illuminate\Database\Seeder;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('currencies')->insert([
           'is_default' => 1,
            'title'     => 'USD',
            'slug'      => 'usd',
            'symbol'    => '$',
            'rate'      => 1,
            'status'    => 1,
            'created_at'=> Carbon\Carbon::now(),
            'updated_at'=> Carbon\Carbon::now(),

        ]);
    }
}
