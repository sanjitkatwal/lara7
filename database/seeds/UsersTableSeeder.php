<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->name = 'admin';
        $user->first_name = 'admin';
        $user->middle_name = 'k';
        $user->last_name = 'k';
        $user->contact = '000000';
        $user->address = 'kathmandu';
        $user->email = 'admin@gmail.com';
        $user->password = bcrypt('admin@123');
        $user->save();

    }
}
