<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = ['image', 'title', 'page_type', 'slug', 'description', 'link','status'];



    /**
     * The menu section that belongs to the page.
     *
     */

    public function menuSections()
    {
        return $this->belongsToMany(MenuSection::class);
    }
}
