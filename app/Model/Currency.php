<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $fillable = ['title', 'slug','is_default', 'rate', 'symbol','rank', 'created_at', 'updated_at', 'status'];

    public function isDeletable(){
        if ($this->slug == 'usd')
            return false;

        return true;
    }
}
