<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = ['image', 'caption', 'alt_text', 'link', 'rank', 'status'];
}
