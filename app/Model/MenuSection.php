<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MenuSection extends Model
{
    protected $fillable = ['title', 'slug', 'hint'];

    /**
     * The pages that belongs to the menu section.
     *
     */

    public function pages()
    {
        return $this->belongsToMany(Page::class);
    }

    public function testFunction()
    {
        return 'test';
    }
}



