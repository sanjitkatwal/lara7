<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Category\AddFormValidation;
use App\Http\Requests\Category\EditFormValidation;
use App\Model\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use function GuzzleHttp\Promise\all;


class CategoryController extends BaseController
{

    protected $base_route = 'admin.category';
    protected $view_path = 'admin.category';
    protected $panel = 'Category';
    protected $folder = 'categories';
    protected $folder_path;


    public function __construct(Category $model)
    {
        $this->model = $model;
        $this->folder_path = 'images'.DIRECTORY_SEPARATOR.$this->folder;
    }

    public function index()
    {
        $data = [];
        $data['rows'] = $this->model->select('id', 'title','parent_id', 'banner_image', 'status', 'description')
            ->orderBy('id', 'desc')->get();

        //dd($data['tows']);
        foreach ($data['rows'] as $row){
            if($row->parent_id == 0){
                $row->parent = '';
            }else{
                $parent = $this->model->find($row->parent_id);
                $row->parent = $parent->title;
            }
        }
        return view(parent::loadCommonDataToView($this->view_path.'.index'), compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        $data['parent_categories'] = $this->getParentCategoriesForSelect();
        return view(parent::loadCommonDataToView($this->view_path.'.create'), compact('data'));
    }

    public function store(AddFormValidation $request)
    {
        // dd($request->all());
        $request->request->add(['slug' => Str::slug($request->get('title'))]);

        // File Uploading
        if ($request->hasFile('image')) {

            $file = $request->file('image');
            $file_name = parent::getRandomStringForImage().'_'.$file->getClientOriginalName();

            //check folder exist or not
            $this->checkDirectoryExist();
            $file->move(public_path($this->folder_path), $file_name);

            $request->request->add(['banner_image' => $file_name]);
        }
        $this->model->create($request->all());

        $request->session()->flash('success_message', $this->panel.'. added successfully.');
        return redirect()->route($this->base_route);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $data = [];
        $data['row'] = $this->model->find($id);
        parent::rowExist($data['row']);

        $data['row'] = $this->model->find($id);

        return view(parent::loadCommonDataToView($this->view_path.'.show'), compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data = [];
        $data['row'] = $this->model->find($id);
        parent::rowExist($data['row']);

        $data['parent_categories'] = $this->getParentCategoriesForSelect();

        return view(parent::loadCommonDataToView($this->view_path.'.edit'), compact('data'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditFormValidation $request, $id)
    {
        $row = $this->model->find($id);
        $request->request->add(['slug' => Str::slug($request->get('title'))]);

        // remove old image
        if ($request->has('remove_image')){
            if ($row->banner_image) {
                if (file_exists(public_path($this->folder_path . DIRECTORY_SEPARATOR . $row->banner_image)))
                    unlink(public_path($this->folder_path . DIRECTORY_SEPARATOR . $row->banner_image));
            }
            $row->banner_image = null;
            $row->save();
        }

        // File Uploading
        if ($request->hasFile('image')) {

            $file = $request->file('image');
            $file_name = parent::getRandomStringForImage().'_'.$file->getClientOriginalName();

            //check folder exist or not
            $this->checkDirectoryExist();
            $file->move(public_path($this->folder_path), $file_name);

            //remove old image
            if ($row->banner_image){
                if(file_exists(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->banner_image)))
                    unlink(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->banner_image));
            }
            $request->request->add(['banner_image' => $file_name]);
        }

        $row->update($request->all());
        $request->session()->flash('success_message', $this->panel.'. updated successfully.');
        return redirect()->route($this->base_route);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $row = $this->model->find($id);
        parent::rowExist($row);

        //remove images
        if($row->banner_image && file_exists(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->banner_image)))
            unlink(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->banner_image));

        $row->delete();
        $request->session()->flash('success_message', $this->panel.'deleted successfully.');
        return redirect()->route($this->base_route);
    }

    protected function getParentCategoriesForSelect(){
        $data = [];
        $data['parent_categories'] = $this->model->where('parent_id', 0)->pluck('title', 'id')->toArray();
        return [0 => '- Is Parent -'] + $data['parent_categories'];
    }
}
