<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Tag\AddFormValidation;
use App\Http\Requests\Tag\EditFormValidation;
use App\Model\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use function GuzzleHttp\Promise\all;


class TagController extends BaseController
{

    protected $base_route = 'admin.tag';
    protected $view_path = 'admin.tag';
    protected $panel = 'Tag';

    public function __construct(Tag $tag)
    {
        $this->model = $tag;
    }

    public function index()
    {
        $data = [];
        $data['rows'] = $this->model->select('id', 'title', 'slug', 'status', 'created_at')
            ->orderBy('id', 'desc')->get();

        return view(parent::loadCommonDataToView($this->view_path.'.index'), compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(parent::loadCommonDataToView($this->view_path.'.create'));
    }

    public function store(AddFormValidation $request)
    {
        // dd($request->all());
        $request->request->add(['slug' => Str::slug($request->get('title'))]);

        $this->model->create($request->all());

        $request->session()->flash('success_message', $this->panel.'. added successfully.');
        return redirect()->route($this->base_route);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data = [];
        $data['row'] = $this->model->find($id);
        parent::rowExist($data['row']);

        return view(parent::loadCommonDataToView($this->view_path.'.edit'), compact('data'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditFormValidation $request, $id)
    {
        $row = $this->model->find($id);
        $request->request->add(['slug' => Str::slug($request->get('title'))]);

        $row->update($request->all());
        $request->session()->flash('success_message', $this->panel.'. updated successfully.');
        return redirect()->route($this->base_route);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $row = $this->model->find($id);
        parent::rowExist($row);


        $row->delete();
        $request->session()->flash('success_message', $this->panel.'deleted successfully.');
        return redirect()->route($this->base_route);
    }

}
