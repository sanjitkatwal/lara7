<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use View;
class BaseController extends Controller
{
    public $model;
    protected function loadCommonDataToView($view_path){

        View::composer($view_path, function ($view){
            $view->with('dashboard_url', route('admin.dashboard'));
            $view->with('_panel', $this->panel);
            $view->with('_base_route', $this->base_route);
            $view->with('_view_path', $this->view_path);
            $view->with('_folder', property_exists($this, 'folder')?$this->folder:'');
        });

        return $view_path;
    }

    public function checkDirectoryExist()
    {
        if(!file_exists(public_path($this->folder_path))){
            mkdir(public_path($this->folder_path));
        }
    }

    public function getRandomStringForImage()
    {
       return rand(2342, 9999);
    }

    public function rowExist($row)
    {
        if(!$row){
            request()->session()->flash('error_message', 'Invalid request.');
            return redirect()->route($this->base_route)->send();
        }
    }
}
