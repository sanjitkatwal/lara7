<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UserController extends BaseController
{

    public function profile()
    {
        $data = [];
        $data['user'] = auth()->user();
        return view(parent::loadCommonDataToView('admin.user.profile'), compact('data'));
    }

    public function profileUpdate(Request $request)
    {
        $this->validateLogin($request);


        $user = auth()->user();
        $user->name = $request->get('name');
        $user->first_name = $request->get('first_name');
        $user->middle_name = $request->get('middle_name');
        $user->last_name = $request->get('last_name');
        $user->contact = $request->get('contact');
        $user->address = $request->get('address');
        $user->email = $request->get('email');

        if($request->get('password')){
            $user->password = bcrypt($request->get('password'));
        }

        $user->save();

        $request->session()->flash('success_message', 'Profile Updated Successfully!');
        return redirect()->route('admin.profile');
    }

    protected function validateLogin(Request $request)
    {
        $rules = [
            'name' => 'required|string',Rule::unique('users')->ignore(auth()->user()->id),
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|email',Rule::unique('users')->ignore(auth()->user()->id),
        ];
        if ($request->get('password')){
            $rules['password'] = 'required|string|min:6|confirmed';
        }

        $this->validate($request, $rules);

//        $this->validate($request, [
//            'name' => 'required|string',
//            'email' => 'required|email',
//            'password' => 'required|string|min:6|confirmed,
//        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['rows'] = User::select('id', 'email', 'first_name', 'middle_name', 'last_name', 'created_at')
            ->orderBy('id', 'desc')->paginate(10);


        return  view(parent::loadCommonDataToView('admin.user.index'), compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $data = [];
        $data['row'] = User::find($id);
        if (!$data['row'])
            return redirect()->route('admin.user');

        return  view(parent::loadCommonDataToView('admin.user.show'), compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
