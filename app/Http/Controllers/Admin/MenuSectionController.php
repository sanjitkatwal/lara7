<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Tag\EditFormValidation;
use App\Model\MenuSection;
use App\Model\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class MenuSectionController extends BaseController
{

    protected $base_route = 'admin.menu-section';
    protected $view_path = 'admin.menu-section';
    protected $panel = 'Menu-Section';

    public function __construct(MenuSection $model)
    {
        $this->model = $model;
    }

    public function index()
    {
        $data = [];
        $data['rows'] = $this->model->select('id', 'title', 'slug', 'hint', 'created_at')->get();

        return view(parent::loadCommonDataToView($this->view_path.'.index'), compact('data'));
    }


    public function edit(Request $request, $id)
    {
        $data = [];
        $data['row'] = $this->model->find($id);
        parent::rowExist($data['row']);
        $data['pages'] = Page::select('id', 'title')->get();

        return view(parent::loadCommonDataToView($this->view_path.'.edit'), compact('data'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditFormValidation $request, $id)
    {


        $row = $this->model->find($id);
        $request->request->add(['slug' => Str::slug($request->get('title'))]);
        $row->update($request->all());

        //$row->pages()->sync($request->get('pages'));
        /*[
            4 => ['rank' => 1],
            3 => ['rank' => 2],
            2 => ['rank' => 3],
        ]*/

        $pvot_data = [];
        foreach ($request->get('pages') as $key => $page_id){
            $pvot_data[$page_id] = [
                'rank'  =>$key +1
            ];
        }
        $row->pages()->sync($pvot_data);


        $request->session()->flash('success_message', $this->panel.'. updated successfully.');
        return redirect()->route($this->base_route);
    }

    public function loadPageRow()
    {
        $data = [];
        $response = [];
        $data['pages'] = Page::select('id', 'title')->get();
        $response['html'] = view($this->view_path.'.common.page_content_row', compact('data'))->render();
        return response()->json(json_encode($response));
    }

    public function test()
    {
        return 'test';
    }

}
