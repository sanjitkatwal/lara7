<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Currency\AddFormValidation;
use App\Http\Requests\Currency\EditFormValidation;
use App\Model\Currency;
use App\Model\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use function GuzzleHttp\Promise\all;


class CurrencyController extends BaseController
{

    protected $base_route = 'admin.currency';
    protected $view_path = 'admin.currency';
    protected $panel = 'Currency';

    public function __construct(Currency $model)
    {
        $this->model = $model;
    }

    public function index()
    {
        $data = [];
        $data['rows'] = $this->model->select('id', 'title', 'slug', 'status','symbol', 'is_default', 'rate', 'created_at')
            ->orderBy('rank', 'asc')->get();

        return view(parent::loadCommonDataToView($this->view_path.'.index'), compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(parent::loadCommonDataToView($this->view_path.'.create'));
    }

    public function store(AddFormValidation $request)
    {
        // dd($request->all());
        $request->request->add(['slug' => Str::slug($request->get('title'))]);

        $this->model->create($request->all());

        $request->session()->flash('success_message', $this->panel.'. added successfully.');
        return redirect()->route($this->base_route);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data = [];
        $data['row'] = $this->model->find($id);
        parent::rowExist($data['row']);

        return view(parent::loadCommonDataToView($this->view_path.'.edit'), compact('data'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditFormValidation $request, $id)
    {
        $row = $this->model->find($id);
        $request->request->add(['slug' => Str::slug($request->get('title'))]);

        $row->update($request->all());
        $request->session()->flash('success_message', $this->panel.'. updated successfully.');
        return redirect()->route($this->base_route);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $row = $this->model->find($id);
        parent::rowExist($row);

        if (!$row->isDeletable()){
            $request->session()->flash('error_message', 'Invalid Request.');
            return redirect()->route($this->base_route);
        }

        $row->delete();
        $request->session()->flash('success_message', $this->panel.'deleted successfully.');
        return redirect()->route($this->base_route);
    }


    public function reorder(Request $request)
    {
        if(!$request->has('id')){
            $request->session()->flash('error_message', 'Invalid Request.');
            return redirect()->route($this->base_route);
        }

        foreach ($request->get('id') as $key => $id) {
            $row = $this->model->find($id);
            $row->rank = $key + 1;
            $row->save();
        }


        $request->session()->flash('success_message', $this->panel.' reorderd successfully.');
        return redirect()->route($this->base_route);
    }

}
