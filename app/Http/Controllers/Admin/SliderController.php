<?php

namespace App\Http\Controllers\Admin;


use App\Http\Requests\Slider\EditFormValidation;
use App\Http\Requests\Slider\AddFormValidation;
use App\Model\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class SliderController extends BaseController
{

    protected $base_route = 'admin.slider';
    protected $view_path = 'admin.slider';
    protected $panel = 'Slider';
    protected $folder = 'Sliders';
    protected $folder_path;


    public function __construct(Slider $model)
    {
        $this->model = $model;
        $this->folder_path = 'images'.DIRECTORY_SEPARATOR.$this->folder;
    }

    public function index()
    {
        $data = [];
        $data['rows'] = $this->model->select('id', 'image','caption', 'created_at', 'rank', 'status')
            ->orderBy('rank', 'asc')->get();

        return view(parent::loadCommonDataToView($this->view_path.'.index'), compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(parent::loadCommonDataToView($this->view_path.'.create'));
    }

    public function store(AddFormValidation $request)
    {

        // File Uploading
        if ($request->hasFile('main_image')) {

            $file = $request->file('main_image');
            $file_name = parent::getRandomStringForImage().'_'.$file->getClientOriginalName();

            //check folder exist or not
            $this->checkDirectoryExist();
            $file->move(public_path($this->folder_path), $file_name);

            $request->request->add(['image' => $file_name]);
        }
        $this->model->create($request->all());

        $request->session()->flash('success_message', $this->panel.'. added successfully.');
        return redirect()->route($this->base_route);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $data = [];
        $data['row'] = $this->model->find($id);
        parent::rowExist($data['row']);

        $data['row'] = $this->model->find($id);

        return view(parent::loadCommonDataToView($this->view_path.'.show'), compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data = [];
        $data['row'] = $this->model->find($id);
        parent::rowExist($data['row']);

        return view(parent::loadCommonDataToView($this->view_path.'.edit'), compact('data'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditFormValidation $request, $id)
    {
        $row = $this->model->find($id);

        // File Uploading
        if ($request->hasFile('main_image')) {

            $file = $request->file('main_image');
            $file_name = parent::getRandomStringForImage().'_'.$file->getClientOriginalName();

            //check folder exist or not
            $this->checkDirectoryExist();
            $file->move(public_path($this->folder_path), $file_name);

            //remove old image
            if ($row->image){
                if(file_exists(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->image)))
                    unlink(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->image));
            }
            $request->request->add(['image' => $file_name]);
        }

        $row->update($request->all());
        $request->session()->flash('success_message', $this->panel.'. updated successfully.');
        return redirect()->route($this->base_route);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $row = $this->model->find($id);
        parent::rowExist($row);

        //remove images
        if($row->image && file_exists(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->image)))
            unlink(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->image));

        $row->delete();
        $request->session()->flash('success_message', $this->panel.'deleted successfully.');
        return redirect()->route($this->base_route);
    }


}
