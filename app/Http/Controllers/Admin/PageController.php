<?php

namespace App\Http\Controllers\Admin;



use App\Http\Requests\Page\AddFormValidation;
use App\Http\Requests\Page\EditFormValidation;
use App\Model\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class PageController extends BaseController
{

    protected $base_route = 'admin.page';
    protected $view_path = 'admin.page';
    protected $panel = 'Page';
    protected $folder = 'Pages';
    protected $folder_path;


    public function __construct(Page $model)
    {
        $this->model = $model;
        $this->folder_path = 'images'.DIRECTORY_SEPARATOR.$this->folder;
    }

    public function index()
    {
        $data = [];
        $data['rows'] = $this->model->select('id', 'image','title', 'created_at', 'slug', 'status')
            ->orderBy('id', 'desc')->paginate(5);

        return view(parent::loadCommonDataToView($this->view_path.'.index'), compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(parent::loadCommonDataToView($this->view_path.'.create'));
    }

    public function store(AddFormValidation $request)
    {
        $request->request->add(['slug' => Str::slug($request->get('title'))]);

//        if($request->page_type == 'link_page'){
//            $request->request->add(['description' => $request->get('link')]);
//        }

        // File Uploading
        if ($request->hasFile('main_image')) {

            $file = $request->file('main_image');
            $file_name = parent::getRandomStringForImage().'_'.$file->getClientOriginalName();

            //check folder exist or not
            $this->checkDirectoryExist();
            $file->move(public_path($this->folder_path), $file_name);

            $request->request->add(['image' => $file_name]);
        }
        $this->model->create($request->all());

        $request->session()->flash('success_message', $this->panel.'. added successfully.');
        return redirect()->route($this->base_route);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $data = [];
        $data['row'] = $this->model->find($id);
        parent::rowExist($data['row']);

        $data['row'] = $this->model->find($id);

        return view(parent::loadCommonDataToView($this->view_path.'.show'), compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data = [];
        $data['row'] = $this->model->find($id);
        parent::rowExist($data['row']);

        return view(parent::loadCommonDataToView($this->view_path.'.edit'), compact('data'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditFormValidation $request, $id)
    {
        $row = $this->model->find($id);

        if($request->get('page_type') != $row->page_type) {
            if ($row->page_type == 'content_page') {
                if ($row->image){
                    if(file_exists(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->image)))
                        unlink(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->image));
                }
                $row->image = null;
                $row->description = null;
                $row->save();

            }elseif($row->page_type == 'link_page') {
                $row->link = null;
                $row->save();
            }
        }

        // File Uploading
        $file_name = '';
        if ($request->hasFile('main_image')) {

            $file = $request->file('main_image');
            $file_name = parent::getRandomStringForImage().'_'.$file->getClientOriginalName();

            //check folder exist or not
            $this->checkDirectoryExist();
            $file->move(public_path($this->folder_path), $file_name);
            $request->request->add(['image' => $file_name]);

            //remove old image
            if ($row->image){
                if(file_exists(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->image)))
                    unlink(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->image));
            }

        }

        if ($request->get('page_type') == 'content_page') {
            $attributes = [
              'page_type'   => $request->get('page_type'),
              'title'       => $request->get('title'),
              'slug'        => Str::slug($request->get('slug')),
              'image'       => $file_name?$file_name:$row->image,
              'description' => $request->get('description'),
              'status'      => $request->get('status'),
            ];
        }else {
            $attributes = [
                'page_type'   => $request->get('page_type'),
                'title'       => $request->get('title'),
                'slug'        => Str::slug($request->get('slug')),
                'link'        => $request->get('link'),
                'status'      => $request->get('status'),
            ];
        }
        $row->update($attributes);

        $request->session()->flash('success_message', $this->panel.'. updated successfully.');
        return redirect()->route($this->base_route);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $row = $this->model->find($id);
        parent::rowExist($row);

        //remove images
        if($row->image && file_exists(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->image)))
            unlink(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->image));

        $row->delete();
        $request->session()->flash('success_message', $this->panel.'deleted successfully.');
        return redirect()->route($this->base_route);
    }


}
