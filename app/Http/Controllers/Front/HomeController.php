<?php

namespace App\Http\Controllers\Front;
use App\Http\Controllers\Controller;

use App\Model\Currency;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $data = [];
        $data['currencies'] = Currency::select('title', 'slug', 'symbol')
            ->where('status', 1)
            ->orderBy('rank')
            ->get();
        return view('shop.home.index', compact('data'));
    }
}
