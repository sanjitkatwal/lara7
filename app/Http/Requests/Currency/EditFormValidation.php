<?php

namespace App\Http\Requests\Currency;

use App\Model\Category;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class EditFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'     => 'required | max:100 | unique:currencies,title,'.request()->get('id'),
            'symbol'    => 'required',
            'rate'      => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.unique' => 'This title has already taken.',
        ];
    }

}
