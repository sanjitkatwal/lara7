<?php

namespace App\Http\Requests\Page;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class AddFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->customValidation();
        return [
            'page_type' => 'link_page_validation',
            'title' => 'required | max:100 | unique:pages,title',

        ];
    }

    public function messages(){
        return [
            'title.unique' => 'This title already exist.',
            'page_type.link_page_validation'    => 'Link is required.'
        ];
    }

    public function customValidation()
    {
        Validator::extend('link_page_validation', function($attribute, $value, $parameters, $validator){
            if ($value == 'link_page') {
                if (!$this->request->get('link'))
                    return false;

                return true;
            }

            return true;

        });
    }

}
