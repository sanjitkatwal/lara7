<?php

namespace App\Http\Requests\Page;

use App\Model\Category;
use App\Model\Page;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class EditFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->customValidation();
        return [
            'page_type' => 'link_page_validation',
            'title' => 'required | max:100 | id_validation | unique:pages,title,'.request()->get('id'),

        ];
    }

    public function messages(){
        return [
            'title.unique' => 'This title already exist.',
            'title.id_validation' => 'Invalid ID Passed.',
            'page_type.link_page_validation'    => 'Link is required.'
        ];
    }


    public function customValidation()
    {
        Validator::extend('id_validation', function($attribute, $value, $parameters, $validators){
            $id = request()->get('id');
            if(Page::find($id))
                return true;

            return false;
        });


        Validator::extend('link_page_validation', function($attribute, $value, $parameters, $validator){
            if ($value == 'link_page') {
                if (!$this->request->get('link'))
                    return false;

                return true;
            }

            return true;

        });
    }
}
