<?php

namespace App\Http\Requests\Tag;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AddFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'     => 'required | max:100| unique:tags,title',

        ];
    }

    public function messages()
    {
        return [
            'title.unique' => 'This title has already taken.'
        ];
    }
    //'name' => 'required|string',Rule::unique('users')->ignore(auth()->user()->id),
}
