<?php

namespace App\Http\Requests\category;

use App\Model\Category;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class EditFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->customValidation();
        return [
            'title'     => 'required | max:100| id_validation | unique:categories,title,'.request()->get('id'),

        ];
    }

    public function messages()
    {
        return [
            'title.unique' => 'This title has already taken.',
            'title.id_validation' => 'The given ID is invalid.',
        ];
    }

    public function customValidation()
    {
        Validator::extend('id_validation', function($attribute, $value, $parameters, $validator){
           $id = request()->get('id');
           if(Category::find($id))
               return true;

           return false;
        });
    }

    /*public function customValidation()
    {
        Validator::extend('minimum_one', function($attribute, $value, $parameters, $validator){
            return $value >= 1?true:false;
        });
    }*/
}
