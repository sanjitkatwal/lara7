<?php

use Illuminate\Support\Facades\Route;




//////////////////////////Admin Part start from here/////////////////////////////
/// Route::group([], function() {
//});
//
//Route::get('admin/dashboard',                       ['as' => 'admin.dashboard',  'uses' => 'Admin\DashboardController@index']);
//Route::get('admin/product',                         ['as' => 'admin.product', 'uses' => 'Admin\ProductController@index']);
//Route::get('admin/product/create',                  ['as' => 'admin.proeuct.create', 'uses' => 'Admin\ProductController@create']);
///Route::group(['prefix' => 'admin'], function() {
//
//});
///

Route::group(['prefix' => 'admin', 'as' =>'admin.', 'namespace' => 'Admin\\', 'middleware' => 'auth'], function() {
    Route::get('dashboard',                       ['as' => 'dashboard',         'uses' => 'DashboardController@index']);
    Route::get('profile',                         ['as' => 'profile',           'uses' => 'UserController@profile']);
    Route::post('profile',                        ['as' => 'profile.update',    'uses' => 'UserController@profileUpdate']);
    Route::get('product',                         ['as' => 'product',           'uses' => 'ProductController@index']);
    Route::get('product/create',                  ['as' => 'proeuct.create',     'uses' => 'ProductController@create']);

    Route::get('user',                            ['as' => 'user',              'uses' => 'UserController@index']);
    Route::get('user/show/{id}',                  ['as' => 'user.show',       'uses' => 'UserController@show']);
    Route::get('user/create',                     ['as' => 'user.create',       'uses' => 'UserController@create']);
    Route::get('user/store',                      ['as' => 'user.store',        'uses' => 'UserController@store']);
    Route::get('user/edit/{id}',                  ['as' => 'user.edit',         'uses' => 'UserController@edit']);
    Route::get('user/update/{id}',                ['as' => 'user.update',       'uses' => 'UserController@update']);
    Route::get('user/delete/{id}',                ['as' => 'user.delete',       'uses' => 'UserController@destroy']);


    Route::get('post',                            ['as' => 'post',              'uses' => 'PostController@index']);
    Route::get('post/create',                     ['as' => 'post.create',       'uses' => 'PostController@create']);
    Route::get('post/store',                      ['as' => 'post.store',        'uses' => 'PostController@store']);
    Route::get('post/edit/{id}',                  ['as' => 'post.edit',         'uses' => 'PostController@edit']);
    Route::get('post/update/{id}',                ['as' => 'post.update',       'uses' => 'PostController@update']);
    Route::get('post/delete/{id}',                ['as' => 'post.delete',       'uses' => 'PostController@destroy']);

    Route::get('tag',                            ['as' => 'tag',              'uses' => 'TagController@index']);
    Route::get('tag/create',                     ['as' => 'tag.create',       'uses' => 'TagController@create']);
    Route::post('tag/store',                     ['as' => 'tag.store',        'uses' => 'TagController@store']);
    Route::get('tag/edit/{id}',                  ['as' => 'tag.edit',         'uses' => 'TagController@edit']);
    Route::post('tag/update/{id}',               ['as' => 'tag.update',       'uses' => 'TagController@update']);
    Route::get('tag/delete/{id}',                ['as' => 'tag.delete',       'uses' => 'TagController@destroy']);

    Route::get('category',                       ['as' => 'category',              'uses' => 'CategoryController@index']);
    Route::get('category/show/{id}',             ['as' => 'category.show',         'uses' => 'CategoryController@show']);
    Route::get('category/create',                ['as' => 'category.create',       'uses' => 'CategoryController@create']);
    Route::post('category/store',                ['as' => 'category.store',        'uses' => 'CategoryController@store']);
    Route::get('category/edit/{id}',             ['as' => 'category.edit',         'uses' => 'CategoryController@edit']);
    Route::post('category/update/{id}',          ['as' => 'category.update',       'uses' => 'CategoryController@update']);
    Route::get('category/delete/{id}',           ['as' => 'category.delete',       'uses' => 'CategoryController@destroy']);

    Route::get('attribute-group',                ['as' => 'attribute-group',              'uses' => 'AttributeController@index']);
    Route::get('attribute-group/show/{id}',      ['as' => 'attribute-group.show',         'uses' => 'AttributeController@show']);
    Route::get('attribute-group/create',         ['as' => 'attribute-group.create',       'uses' => 'AttributeController@create']);
    Route::post('attribute-group/store',         ['as' => 'attribute-group.store',        'uses' => 'AttributeController@store']);
    Route::get('attribute-group/edit/{id}',      ['as' => 'attribute-group.edit',         'uses' => 'AttributeController@edit']);
    Route::post('attribute-group/update/{id}',   ['as' => 'attribute-group.update',       'uses' => 'AttributeController@update']);
    Route::get('attribute-group/delete/{id}',    ['as' => 'attribute-group.delete',       'uses' => 'AttributeController@destroy']);

    Route::get('slider',                          ['as' => 'slider',                        'uses' => 'SliderController@index']);
    Route::get('slider/show/{id}',                ['as' => 'slider.show',                   'uses' => 'SliderController@show']);
    Route::get('slider/create',                   ['as' => 'slider.create',                 'uses' => 'SliderController@create']);
    Route::post('slider/store',                   ['as' => 'slider.store',                  'uses' => 'SliderController@store']);
    Route::get('slider/edit/{id}',                ['as' => 'slider.edit',                   'uses' => 'SliderController@edit']);
    Route::post('slider/update/{id}',             ['as' => 'slider.update',                 'uses' => 'SliderController@update']);
    Route::get('slider/delete/{id}',              ['as' => 'slider.delete',                 'uses' => 'SliderController@destroy']);

    Route::get('page',                          ['as' => 'page',                        'uses' => 'PageController@index']);
    Route::get('page/show/{id}',                ['as' => 'page.show',                   'uses' => 'PageController@show']);
    Route::get('page/create',                   ['as' => 'page.create',                 'uses' => 'PageController@create']);
    Route::post('page/store',                   ['as' => 'page.store',                  'uses' => 'PageController@store']);
    Route::get('page/edit/{id}',                ['as' => 'page.edit',                   'uses' => 'PageController@edit']);
    Route::post('page/update/{id}',             ['as' => 'page.update',                 'uses' => 'PageController@update']);
    Route::get('page/delete/{id}',              ['as' => 'page.delete',                 'uses' => 'PageController@destroy']);

    Route::get('menu-section',                  ['as' => 'menu-section',                 'uses' => 'MenuSectionController@index']);
    Route::get('menu-section/edit/{id}',        ['as' => 'menu-section.edit',            'uses' => 'MenuSectionController@edit']);
    Route::post('menu-section/update/{id}',     ['as' => 'menu-section.update',          'uses' => 'MenuSectionController@update']);
    Route::post('menu-section/load-page-row',   ['as' => 'menu-section.load-page-row',    'uses' => 'MenuSectionController@loadPageRow']);

    Route::get('brand',                          ['as' => 'brand',                        'uses' => 'BrandController@index']);
    Route::get('brand/show/{id}',                ['as' => 'brand.show',                   'uses' => 'BrandController@show']);
    Route::get('brand/create',                   ['as' => 'brand.create',                 'uses' => 'BrandController@create']);
    Route::post('brand/store',                   ['as' => 'brand.store',                  'uses' => 'BrandController@store']);
    Route::get('brand/edit/{id}',                ['as' => 'brand.edit',                   'uses' => 'BrandController@edit']);
    Route::post('brand/update/{id}',             ['as' => 'brand.update',                 'uses' => 'BrandController@update']);
    Route::get('brand/delete/{id}',              ['as' => 'brand.delete',                 'uses' => 'BrandController@destroy']);

    Route::get('currency',                          ['as' => 'currency',                        'uses' => 'CurrencyController@index']);
    Route::get('currency/show/{id}',                ['as' => 'currency.show',                   'uses' => 'CurrencyController@show']);
    Route::get('currency/create',                   ['as' => 'currency.create',                 'uses' => 'CurrencyController@create']);
    Route::post('currency/store',                   ['as' => 'currency.store',                  'uses' => 'CurrencyController@store']);
    Route::get('currency/edit/{id}',                ['as' => 'currency.edit',                   'uses' => 'CurrencyController@edit']);
    Route::post('currency/update/{id}',             ['as' => 'currency.update',                 'uses' => 'CurrencyController@update']);
    Route::get('currency/delete/{id}',              ['as' => 'currency.delete',                 'uses' => 'CurrencyController@destroy']);
    Route::post('currency/reorder',                 ['as' => 'currency.reorder',                 'uses' => 'CurrencyController@reorder']);

});





// --------------------------------------Frontend Routes -------------------------

Auth::routes();

Route::get('/', 'Front\HomeController@index')->name('home');
